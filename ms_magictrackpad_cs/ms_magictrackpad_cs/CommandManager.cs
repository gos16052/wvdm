﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ms_magictrackpad_cs
{
    class CommandManager
    {
        public const byte CMD_1_CLICK = 10;
        public const byte CMD_1_MOVE = 11;
        public const byte CMD_1_TOUCHDOWN = 12;
        public const byte CMD_1_TOUCHUP = 13;
        public const byte CMD_2_RIGHTCLICK = 21;
        public const byte CMD_2_VIEWCONTROL = 22;
        public const byte CMD_2_PAGECONTROL = 23;
        public const byte CMD_3_PROGRAMLIST = 31;
        public const byte CMD_3_CHANEWINDOW = 32;
        public const byte CMD_4_BACKGROUND = 41;
        public const byte CMD_4_FULLSCREEN = 42;

        /*
        private static int getInt(byte[] buffer, int index)
        {
            byte[] temp = new byte[4];
            for(int i=index, j=0; i<index +4; i++, j++)
            {
                temp[j] = buffer[i];
            }
            return System.BitConverter.ToInt32(temp, 0);
        }
        */

        private static byte getByte(byte[] cmd, int index)
        {
            return cmd[index];
            /*
            byte[] input = new byte[32];
            if (BTManager.Receive(input) <= 0)
            {
                Form1.log("closed");
                return 0;
            }
            if (input.Length == 0) return 0;
            

            Form1.log("buffer " + BitConverter.ToString(input));
            return (byte)input[0];
            */
        }
        
        private static int getCoord(byte[] cmd, int index)
        {
            byte[] temp = new byte[4];
            for (int i = index, j=3; i< index + 4; i++) { 
                temp[j] = cmd[i];
                j--;
            }
            return BitConverter.ToInt32(temp, 0);
            /*
            int b = getByte();
            if (b >= 128)
            {
                b = b ^ 0xFF;
                return -(b + 1);
            }
            else
                return b;
            */
        }

        const int start = 8;
        const int max = 8;
        static int a = start;
        public static StringBuilder getTestCmd()
        {
            Thread.Sleep(10000);
            StringBuilder s = null;
            switch (a)
            {
                case 1:
                    s = GenCmd(10, 0, 0, 255);
                    break;
                case 2:
                    s = GenCmd(11, 5, 5, 254);
                    break;
                case 3:
                    s = GenCmd(21, 0, 0, 255);
                    break;
                case 4:
                    s = GenCmd(23, 0, 0, 1);
                    break;
                case 5:
                    s = GenCmd(31, 0, 0, 1);
                    break;
                case 6:
                    s = GenCmd(32, 0, 0, 2);
                    break;
                case 7:
                    s = GenCmd(41, 0, 0, 255);
                    break;
                case 8:
                    s = GenCmd(42, 0, 0, 255);
                    break;
            }
            a = a + 1 > max ? 1 : a + 1;
            return s;
        }
        public static StringBuilder GenCmd(byte cmd, float x, float y, byte dir)
        {
            char[] buffer = new char[32];
            buffer[0] = (char)cmd;
            int j = 1;
            switch (dir)
            {
                case 255: // no extends
                    break;
                case 254: // with float
                    byte[] bx = BitConverter.GetBytes(x); ;
                    byte[] by = BitConverter.GetBytes(y); ;
                    for (int i=0; i<4; i++,j++)
                    {
                        buffer[j] = (char)bx[i];
                    }
                    for (int i = 0; i < 4; i++, j++)
                    {
                        buffer[j] = (char)by[i];
                    }
                    break;
                    // with byte    
                default:
                    buffer[1] = (char)dir;
                    break;
            }
            return (new StringBuilder()).Append(buffer);
        }

        public static void cmd(byte[] cmd)
        { // dx dy 에 스케일 넣기
            byte direction, action;
            int dx, dy, x, y;
            switch (cmd[0]) {
                case CMD_1_CLICK:
                    VWindow.Click();
                    Form1.log("click");
                    break;
                case CMD_1_MOVE:
                    x = getCoord(cmd, 1);
                    y = getCoord(cmd, 5);
                    VWindow.Move(x, y);
                    Form1.log("move " + x.ToString() + " " + y.ToString());
                    break;
                case CMD_1_TOUCHDOWN:
                    VWindow.TouchDown();
                    Form1.log("touchdown");
                    break;
                case CMD_1_TOUCHUP:
                    VWindow.TouchUp();
                    Form1.log("touchup");
                    break;
                case CMD_2_RIGHTCLICK:
                    VWindow.RightClick();
                    Form1.log("rightclick");
                    break;
                case CMD_2_VIEWCONTROL:
                    dx = getCoord(cmd, 1);
                    dy = getCoord(cmd, 5);
                    VWindow.ViewControl(dx, dy); // not yet implement
                    break;
                case CMD_2_PAGECONTROL:
                    direction = getByte(cmd, 1);
                    if (direction == 0) break;
                    VWindow.PageControl(direction);
                    Form1.log("pagecontrol" + direction.ToString());
                    break;
                case CMD_3_PROGRAMLIST:
                    action = getByte(cmd, 1);
                    if (action == 0) break;
                    VWindow.ProgramList(action);
                    Form1.log("program list" + action.ToString());
                    break;
                case CMD_3_CHANEWINDOW:
                    direction = getByte(cmd, 1);
                    if (direction == 0) break;
                    VWindow.ChangeWindow(direction);
                    Form1.log("changewindow" + direction.ToString());
                    break;
                case CMD_4_BACKGROUND:
                    VWindow.Background();
                    Form1.log("background");
                    break;
                case CMD_4_FULLSCREEN:
                    VWindow.FullScreen();
                    Form1.log("fullscreen");
                    break;
            }

        }
        
    }
}


/*
    명령어(1B) | extend(31B)

    명령어정의
    ------------------------------------------------------------
    | cmd | extend             | #touch | operation
    |-----|--------------------|--------|-----------------------
                                                                                        |  1  |                    | 0      | 해상도 요청
    | 10  |                    | 1      | 클릭
    | 11  | dx(8B) dy(8B)      | 1      | 이동
    | 21  |                    | 2      | 오른쪽클릭
    | 22  | dx(8B) dy(8B)      | 2      | 페이지보기 위치이동(2터치 상하좌우)          viewcontrol
    | 23  | LEFT:1,RIGHT:2(1B) | 2      | 이전페이지/다음페이지                       pagecontrol
    | 31  | UP:1,DOWN:2(1B)    | 3      | 프로그램리스트 ON/OFF                        
    | 32  | LEFT:1,RIGHT:2(1B) | 3      | 윈도우전환
    | 41  |                    | 4-5    | 바탕화면
    | 42  |                    | 4-5    | 프로그램전체화면
    ------------------------------------------------------------


 */

