﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ms_magictrackpad_cs
{
    public static class BTManager
    {
        [DllImport("btlib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Conn();

        [DllImport("btlib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ConnClose();

        [DllImport("btlib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Init();

        [DllImport("btlib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Receive(byte[] buf_32);

        [DllImport("btlib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetLastError(byte[] buf_32);
    }
}
