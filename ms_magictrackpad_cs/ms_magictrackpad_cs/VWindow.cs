﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsDesktop;
using WindowsDesktop.Interop;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace ms_magictrackpad_cs
{
    

    /*  https://stackoverflow.com/questions/32416843/altering-win10-virtual-desktop-behavior
    internal static class Guids
    {
        public static readonly Guid CLSID_ImmersiveShell =
            new Guid(0xC2F03A33, 0x21F5, 0x47FA, 0xB4, 0xBB, 0x15, 0x63, 0x62, 0xA2, 0xF2, 0x39);
        public static readonly Guid CLSID_VirtualDesktopManagerInternal =
            new Guid(0xC5E0CDCA, 0x7B6E, 0x41B2, 0x9F, 0xC4, 0xD9, 0x39, 0x75, 0xCC, 0x46, 0x7B);
        public static readonly Guid CLSID_VirtualDesktopManager =
            new Guid("AA509086-5CA9-4C25-8F95-589D3C07B48A");
        public static readonly Guid IID_IVirtualDesktopManagerInternal =
            new Guid("AF8DA486-95BB-4460-B3B7-6E7A6B2962B5");
        public static readonly Guid IID_IVirtualDesktop =
            new Guid("FF72FFDD-BE7E-43FC-9C03-AD81681E88E4");
    }

    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("FF72FFDD-BE7E-43FC-9C03-AD81681E88E4")]
    public interface IVirtualDesktop
    {
        void notimpl1(); // void IsViewVisible(IApplicationView view, out int visible);
        Guid GetId();
    }

    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("AF8DA486-95BB-4460-B3B7-6E7A6B2962B5")]
    public interface IVirtualDesktopManagerInternal
    {
        int GetCount();
        void notimpl1();  // void MoveViewToDesktop(IApplicationView view, IVirtualDesktop desktop);
        void notimpl2();  // void CanViewMoveDesktops(IApplicationView view, out int itcan);
        IVirtualDesktop GetCurrentDesktop();
        void GetDesktops(out IObjectArray desktops);
        [PreserveSig]
        int GetAdjacentDesktop(IVirtualDesktop from, int direction, out IVirtualDesktop desktop);
        void SwitchDesktop(IVirtualDesktop desktop);
        IVirtualDesktop CreateDesktop();
        void RemoveDesktop(IVirtualDesktop desktop, IVirtualDesktop fallback);
        IVirtualDesktop FindDesktop(ref Guid desktopid);
    }

    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("a5cd92ff-29be-454c-8d04-d82879fb3f1b")]
    internal interface IVirtualDesktopManager
    {
        int IsWindowOnCurrentVirtualDesktop(IntPtr topLevelWindow);
        Guid GetWindowDesktopId(IntPtr topLevelWindow);
        void MoveWindowToDesktop(IntPtr topLevelWindow, ref Guid desktopId);
    }

    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("92CA9DCD-5622-4bba-A805-5E9F541BD8C9")]
    public interface IObjectArray
    {
        void GetCount(out int count);
        void GetAt(int index, ref Guid iid, [MarshalAs(UnmanagedType.Interface)]out object obj);
    }

    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("6D5140C1-7436-11CE-8034-00AA006009FA")]
    internal interface IServiceProvider10
    {

        [return: MarshalAs(UnmanagedType.I4)]
        int QueryService(ref Guid service, ref Guid riid, out object obj);
    }
    public class Desktop
    {
        public static int Count
        {
            // Returns the number of desktops
            get { return DesktopManager.Manager.GetCount(); }
        }

        public static Desktop Current
        {
            // Returns current desktop
            get { return new Desktop(DesktopManager.Manager.GetCurrentDesktop()); }
        }

        public static Desktop FromIndex(int index)
        {
            // Create desktop object from index 0..Count-1
            return new Desktop(DesktopManager.GetDesktop(index));
        }

        public static Desktop FromWindow(IntPtr hWnd)
        {
            // Creates desktop object on which window <hWnd> is displayed
            Guid id = DesktopManager.WManager.GetWindowDesktopId(hWnd);
            return new Desktop(DesktopManager.Manager.FindDesktop(ref id));
        }

        public static Desktop Create()
        {
            // Create a new desktop
            return new Desktop(DesktopManager.Manager.CreateDesktop());
        }

        public void Remove(Desktop fallback = null)
        {
            // Destroy desktop and switch to <fallback>
            var back = fallback == null ? DesktopManager.GetDesktop(0) : fallback.itf;
            DesktopManager.Manager.RemoveDesktop(itf, back);
        }

        public bool IsVisible
        {
            // Returns <true> if this desktop is the current displayed one
            get { return object.ReferenceEquals(itf, DesktopManager.Manager.GetCurrentDesktop()); }
        }

        public void MakeVisible()
        {
            // Make this desktop visible
            DesktopManager.Manager.SwitchDesktop(itf);
        }

        public Desktop Left
        {
            // Returns desktop at the left of this one, null if none
            get
            {
                IVirtualDesktop desktop;
                int hr = DesktopManager.Manager.GetAdjacentDesktop(itf, 3, out desktop);
                if (hr == 0) return new Desktop(desktop);
                else return null;

            }
        }

        public Desktop Right
        {
            // Returns desktop at the right of this one, null if none
            get
            {
                IVirtualDesktop desktop;
                int hr = DesktopManager.Manager.GetAdjacentDesktop(itf, 4, out desktop);
                if (hr == 0) return new Desktop(desktop);
                else return null;
            }
        }

        public void MoveWindow(IntPtr handle)
        {
            // Move window <handle> to this desktop
            DesktopManager.WManager.MoveWindowToDesktop(handle, itf.GetId());
        }

        public bool HasWindow(IntPtr handle)
        {
            // Returns true if window <handle> is on this desktop
            return itf.GetId() == DesktopManager.WManager.GetWindowDesktopId(handle);
        }

        public override int GetHashCode()
        {
            return itf.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            var desk = obj as Desktop;
            return desk != null && object.ReferenceEquals(this.itf, desk.itf);
        }

        private IVirtualDesktop itf;
        private Desktop(IVirtualDesktop itf) { this.itf = itf; }
        public IVirtualDesktop getItf() { return this.itf; }
    }

    internal static class DesktopManager
    {
        static DesktopManager()
        {
            var shell = (IServiceProvider10)Activator.CreateInstance(Type.GetTypeFromCLSID(Guids.CLSID_ImmersiveShell));
            //Manager = shell.QueryService(new Guid("c5e0cdca-7b6e41b2-9fc4-d93975cc467b"), typeof.GUID);
            object k;
            //Manager = (IVirtualDesktopManagerInternal)shell.QueryService(Guids.CLSID_VirtualDesktopManagerInternal, Guids.IID_IVirtualDesktopManagerInternal);
            shell.QueryService(new Guid("c5e0cdca-7b6e-41b2-9fc4-d93975cc467b"), new Guid("ef9f1a6c-d3cc-4358-b712-f84b635bebe7"), out k);
//            Manager = (IVirtualDesktopManagerInternal)
            WManager = (IVirtualDesktopManager)Activator.CreateInstance(Type.GetTypeFromCLSID(Guids.CLSID_VirtualDesktopManager));
        }
        
        internal static IVirtualDesktop GetDesktop(int index)
        {
            int count = Manager.GetCount();
            if (index < 0 || index >= count) throw new ArgumentOutOfRangeException("index");
            IObjectArray desktops;
            Manager.GetDesktops(out desktops);
            object objdesk;
            desktops.GetAt(index, Guids.IID_IVirtualDesktop, out objdesk);
            Marshal.ReleaseComObject(desktops);
            return (IVirtualDesktop)objdesk;
        }

        public static IVirtualDesktop[] GetDesktops()
        {
            IObjectArray objs;
            Manager.GetDesktops(out objs);
            int cnt;
            objs.GetCount(out cnt);
            object[] desktops = new IVirtualDesktop[cnt];
            for (int i = 0; i < cnt; i++) {
                objs.GetAt(i, Guids.IID_IVirtualDesktop, out desktops[i]);
            }
            return (IVirtualDesktop[])desktops;
        }

        internal static IVirtualDesktopManagerInternal Manager;
        internal static IVirtualDesktopManager WManager;
    }
    */
    class LibFunc
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        public static VirtualDesktop GetDesktop()
        {
            IntPtr handle = GetForegroundWindow();

            return VirtualDesktop.FromHwnd(handle);
        }

        [DllImport("user32.dll", EntryPoint = "GetSystemMetrics")]
        public static extern int GetSystemMetrics(int which);

        internal struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        //[DllImport("user32.dll")]
        //public static extern void SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter, int X, int Y, int width, int height, uint flags);

        [DllImport("user32.dll")]
        public static extern bool SetWindowPlacement(IntPtr hwnd, ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll")]
        public static extern bool GetWindowPlacement(IntPtr hwnd, ref WINDOWPLACEMENT lpwndpl);

        private const int SM_CXSCREEN = 0;
        private const int SM_CYSCREEN = 1;
        private static IntPtr HWND_TOP = IntPtr.Zero;
        private const int SWP_SHOWWINDOW = 64; // 0x0040

        public static int ScreenX
        {
            get { return GetSystemMetrics(SM_CXSCREEN); }
        }

        public static int ScreenY
        {
            get { return GetSystemMetrics(SM_CYSCREEN); }
        }

        public static void SetWinFullScreen(IntPtr hwnd)
        {
            // https://msdn.microsoft.com/ko-kr/library/windows/desktop/ms632611.aspx 참조

            WINDOWPLACEMENT wndpl = new WINDOWPLACEMENT();
            GetWindowPlacement(hwnd, ref wndpl);
            //wndpl.flags = WPF_RESTORETOMAXIMIZED;
            wndpl.showCmd = 3; // SW_MAXIMIZE
            SetWindowPlacement(hwnd, ref wndpl);

            //SetWindowPos(hwnd, HWND_TOP, 0, 0, ScreenX, ScreenY, SWP_SHOWWINDOW);
            // https://msdn.microsoft.com/ko-kr/library/windows/desktop/ff468919.aspx
        }

        [DllImport("user32.dll")]
        private static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        private const int KEYEVENTF_EXTENDEDKEY = 1;
        private const int KEYEVENTF_KEYUP = 2;

        public static void KeyDown(Keys vKey)
        {
            keybd_event((byte)vKey, 0, KEYEVENTF_EXTENDEDKEY, 0);
        }

        public static void KeyUp(Keys vKey)
        {
            keybd_event((byte)vKey, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
        }

        [DllImport("user32.dll")]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        public delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);
        [DllImport("user32.dll")]
        public static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, WinEventDelegate lpfnWinEventProc, uint idProcess, uint idThread, uint dwFlags);

        //[DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        [DllImport("user32.dll")]
        public static extern void mouse_event(uint dwFlags, int dx, int dy, uint dwData, int dwExtraInfo);
        public const int MOUSEEVENTF_ABSOLUTE = 0x8000;
        public const int MOUSEEVENTF_LEFTDOWN = 0x0002;
        public const int MOUSEEVENTF_LEFTUP = 0x0004;
        public const int MOUSEEVENTF_MIDDLEDOWN = 0x0020;
        public const int MOUSEEVENTF_MIDDLEUP = 0x0040;
        public const int MOUSEEVENTF_MOVE = 0x0001;
        public const int MOUSEEVENTF_RIGHTDOWN = 0x0008;
        public const int MOUSEEVENTF_RIGHTUP = 0x0010;
        public const int MOUSEEVENTF_WHEEL = 0x0800;
        public const int MOUSEEVENTF_XDOWN = 0x0080;
        public const int MOUSEEVENTF_XUP = 0x0100;
        public const int MOUSEEVENTF_HWHEEL = 0x01000;

        public delegate bool EnumWindowsProc(IntPtr hWnd, int lParam);
        [DllImport("user32.dll")]
        public static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);

        [DllImport("user32.dll")]
        public static extern bool IsWindowVisible(IntPtr hWnd);


        [DllImport("user32.dll")]
        public static extern int GetWindowTextLength(IntPtr hWnd);
    }

    public class VWindow
    {
        private static bool isBackground = false;
        private static bool isWindowTab = false;

        public const byte DIRECTION_UP = 1;
        public const byte DIRECTION_RIGHT = 2;
        public const byte DIRECTION_DOWN = 3;
        public const byte DIRECTION_LEFT = 4;

        public static void start()
        {
            LibFunc.GetDesktop();
        }

        public static void Click()
        {
            LibFunc.mouse_event(
                LibFunc.MOUSEEVENTF_LEFTDOWN | LibFunc.MOUSEEVENTF_LEFTUP,
                Cursor.Position.X, 
                Cursor.Position.Y, 
                0, 
                0
            );
        }

        public static void RightClick()
        {
            LibFunc.mouse_event(
                LibFunc.MOUSEEVENTF_RIGHTDOWN | LibFunc.MOUSEEVENTF_RIGHTUP,
                Cursor.Position.X,
                Cursor.Position.Y,
                0,
                0
            );
        }

        public static void TouchDown()
        {
            LibFunc.mouse_event(
                LibFunc.MOUSEEVENTF_LEFTDOWN,
                Cursor.Position.X,
                Cursor.Position.Y,
                0,
                0
            );
        }

        public static void TouchUp()
        {
            LibFunc.mouse_event(
                LibFunc.MOUSEEVENTF_LEFTUP,
                Cursor.Position.X,
                Cursor.Position.Y,
                0,
                0
            );
        }
        public static void Move(int dx, int dy)
        {
            if (Math.Abs(dx)> 100 || Math.Abs(dy) > 100) return;
            // dx에 scale을 넣어야할것같다
            //Form1.log("cur pos " + Cursor.Position.X + " " + Cursor.Position.Y);
            Cursor.Position = new System.Drawing.Point(Cursor.Position.X + dx, Cursor.Position.Y + dy);
        }

        public static void ViewControl(float dx, float dy)
        {
            LibFunc.mouse_event(
                LibFunc.MOUSEEVENTF_MIDDLEDOWN | LibFunc.MOUSEEVENTF_MIDDLEUP,
                (int)dx,
                (int)dy,
                0,
                0
            );
        }

        public static void PageControl(byte direction)
        {
            if (direction == DIRECTION_LEFT)
            {
                LibFunc.KeyDown(Keys.Alt);
                LibFunc.KeyDown(Keys.Left);
                LibFunc.KeyUp(Keys.Alt);
                LibFunc.KeyUp(Keys.Left);
            }
            else if (direction == DIRECTION_RIGHT)
            {
                LibFunc.KeyDown(Keys.Alt);
                LibFunc.KeyDown(Keys.Right);
                LibFunc.KeyUp(Keys.Alt);
                LibFunc.KeyUp(Keys.Right);
            }
        }

        public static void ProgramList(byte direction)
        {
            WindowTab();
        }

        public static void ChangeWindow(byte direction)
        {
            if (direction == DIRECTION_LEFT)
            {
                WindowLeft();
            }
            else if (direction == DIRECTION_RIGHT)
            {
                WindowRight();
            }
        }

        // 3터치 왼쪽 스와이프 
        public static void WindowLeft()
        {
            try
            {
                VirtualDesktop d = LibFunc.GetDesktop();
                VirtualDesktop ld = LibFunc.GetDesktop().GetLeft();
                ld.Switch();
            }
            catch (NullReferenceException e)
            {
                Form1.log(e.Message);
            }
        }

        // 3터치 오른쪽 스와이프
        public static void WindowRight()
        {
            try
            {
                VirtualDesktop d = LibFunc.GetDesktop();
                VirtualDesktop rd = d.GetRight();
                rd.Switch();
            }
            catch (NullReferenceException e)
            {
                Form1.log(e.Message);
            }
        }
        
        // 3터치 위/아래로 스와이프
        public static void WindowTab()
        {
            LibFunc.KeyDown(Keys.LWin);
            LibFunc.KeyDown(Keys.Tab);
            LibFunc.KeyUp(Keys.LWin);
            LibFunc.KeyUp(Keys.Tab);
        }
        
        // 4터치 가운데 펴기
        public static void Background()
        {
            LibFunc.KeyDown(Keys.LWin);
            LibFunc.KeyDown(Keys.D);
            LibFunc.KeyUp(Keys.LWin);
            LibFunc.KeyUp(Keys.D);
            isBackground = true;
        }
        /*
        static LibFunc.WinEventDelegate rmWindow = delegate (
            IntPtr hWinEventHook, 
            uint eventType, 
            IntPtr chwnd, 
            int idObject, 
            int idChild, 
            uint dwEventThread, 
            uint dwmsEventTime
        )
        {
            int index = FullWnd.IndexOf(chwnd);
            if( index != -1)
            {
                FullDesktop[index].Remove();
                FullDesktop.RemoveAt(index);
            }
        };
        */

        static List<VirtualDesktop> desktops = new List<VirtualDesktop>();
        static List<Boolean> isUserCreated = new List<Boolean>();

        public static Thread GCDesktop = new Thread(delegate ()
        {   /* get existing desktops */

            VirtualDesktop[] desktops;
            VirtualDesktop temp;
            bool[] cnts;
            while (true)
            {
                Thread.Sleep(1000);

                desktops = VirtualDesktop.GetDesktops();
                
                cnts = new bool[desktops.Length];
                Array.Clear(cnts, 0, cnts.Length);

                LibFunc.EnumWindows(new LibFunc.EnumWindowsProc( delegate (IntPtr hWnd, int lParam)
                {
                    if (!LibFunc.IsWindowVisible(hWnd)) return true;
                    if (LibFunc.GetWindowTextLength(hWnd) == 0) return true;
                    
                    temp = VirtualDesktop.FromHwnd(hWnd);
                    for (int i = 0; i < desktops.Length; i++)
                    {
                        if (temp == desktops[i])
                        {
                            cnts[i] = true;
                        }
                    }
                    return true;
                }), 0);

                
                for(int i=0; i<desktops.Length; i++)
                {
                    if(cnts[i] == false)
                    {
                        if(VirtualDesktop.Current == desktops[i])
                        {
                            desktops[i].Remove();
                        }
                        else
                        {
                            desktops[i].Remove(VirtualDesktop.Current);
                        }
                    }
                }
            }
        });

        // 컨트롤 알트 F, 키 후킹 필요.
        public static void FullScreen()
        {
            try
            {
                VirtualDesktop newd = VirtualDesktop.Create();          //Vwindow만듦 
                IntPtr hwnd = LibFunc.GetForegroundWindow();            //현재 핸들 가져옴
                VirtualDesktopHelper.MoveToDesktop(hwnd, newd);         //새로만든 VWindow에 옮김 
                newd.Switch();                                          //VWindow 전환 

                LibFunc.SetWinFullScreen(hwnd);                         //전체화면
            }
            catch (COMException e) { Form1.log("Com: " + e.Message); }
            //catch (NullReferenceException e2) { Form1.log("Null: " + e2.Message); }
            //uint pid = 0;
            //int tid = LibFunc.GetWindowThreadProcessId(hwnd, out pid);        //핸들로 pid, tid가져옴

            //desktops.Add(newd);
        }
    }
}

/*
// Get all Virtual Desktops
var desktops = VirtualDesktop.GetDesktops();

// Get Virtual Desktop for specific window
var desktop = VirtualDesktop.FromHwnd(hwnd);

// Get the left/right desktop
var left  = desktop.GetLeft();
var right = desktop.GetRight();
Manage Virtual Desktops:

// Create new
var desktop = VirtualDesktop.Create();

// Remove
desktop.Remove();

// Switch
desktop.Switch();
for WPF window


/* Need to install 'VirtualDesktop.WPF' package * /
// Check whether a window is on the current desktop.
var isCurrent = window.IsCurrentVirtualDesktop();

// Get Virtual Desktop for WPF window
var desktop = window.GetCurrentDesktop();

// Move window to specific Virtual Desktop
window.MoveToDesktop(desktop);
 */

