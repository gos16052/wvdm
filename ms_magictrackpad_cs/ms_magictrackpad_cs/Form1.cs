﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ms_magictrackpad_cs
{
    public partial class Form1 : Form
    {
        delegate void VMethod();
        delegate void OffThread(Boolean isUSBConnected);

        Thread USBThread;
        Thread BTThread;
        public TextBox tbox = null;
        
        public static Form1 self = null;

        public static void log(String msg)
        {
            if (self == null || self.tbox == null) return;
            self.tbox.AppendText(msg);
            self.tbox.AppendText(Environment.NewLine);
        }

        public static void nllog(String msg)
        {
            if (self == null || self.tbox == null) return;
            self.tbox.AppendText(msg);
        }

        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;

            self = this;
            InitializeComponent();
            //HideForm();
            main();
            this.Size = new Size(1000, 800);
            VWindow.GCDesktop.Start();

            Button tButton = new Button
            {
                Width = 100,
                Location = new Point(0, 0),
                Text = "bttest"
            };
            tButton.Click +=  delegate (object sender, EventArgs e)
            {
                OffThread callback = delegate (Boolean isUSBConnected)
                {
                    if (isUSBConnected)
                    {
                        BTThread.Abort(); // memory leak is occured
                        BTThread = null; // evict the instance for GC

                        MessageBox.Show("via USB");
                        
                        // TODO USBTHREAD
                    }
                    else
                    {
                        USBThread.Abort(); // memory leak is occured
                        USBThread = null; // evict the instance for GC


                        Form1.log("bt thread connected");
                        // TODO BTTHREAD
                        StringBuilder input = new StringBuilder(32);
                        byte[] buffer = new byte[32];
                        byte cmd;
                        while (true)
                        {/*
                            input = CommandManager.getTestCmd();
                            */
                            if (BTManager.Receive(buffer) <= 0)
                            {// connection is closed.
                                Form1.log("closed");
                                break;
                            }

                            /*
                             * byte[] before = new byte[32];
                            for (int i=0; i<32; i++)
                            {
                                if (i < temp.Length)
                                {
                                    buffer[i] = (byte)(temp[i] ^ 0x80);
                                    before[i] = (byte)temp[i];

                                }
                                else
                                {
                                    buffer[i] = 0;
                                    before[i] = (byte)0;
                                }
                                
                            }
                            Form1.log("before "+BitConverter.ToString(before));
                            
                            */
                            if(buffer[0] > CommandManager.CMD_1_MOVE)
                                Form1.log("buffer " + BitConverter.ToString(buffer));
                            
                            CommandManager.cmd(buffer);

                            //MessageBox.Show("input "+ input.ToString());
                        }
                    }
                };

                USBThread = new Thread(delegate () 
                {
                    USBManager.usb_func();
                    //callback(true);
                });
                BTThread = new Thread(delegate ()
                {
                    Form1.log("bt thread start");
                    BTManager.Init();
                    BTManager.Conn();
                    callback(false);
                });

                //USBThread.Start();

                VWindow.start();
                BTThread.Start();
                /*
                new Thread(delegate ()
                {
                    callback(false);
                }).Start();
                */
            };
            //buttons.Add(tButton);
            this.Controls.Add(tButton);
            tButton.PerformClick();

            this.Height = 500;

            tbox = new TextBox
            {
                Width = 900,
                Height = 400,
                MaxLength = 70,
                //ReadOnly = true,
                Location = new Point(0, 50),
                ScrollBars = ScrollBars.Vertical,
                Multiline = true
            };
            
            this.Controls.Add(tbox);
            
            List<VMethod> VMethods = new List<VMethod>();

            // 버튼에 표시할 함수이름
            string[] FuncNames = {
            /*    "WindowLeft",
                "WindowRight",
             */   "FullScreen"/*,
                "Background",
                "WindowTab"*/
            };
            // 메소드포인터(?)
            /*
            VMethods.Add(VWindow.WindowLeft);
            VMethods.Add(VWindow.WindowRight);*/
            VMethods.Add(VWindow.FullScreen);/*
            VMethods.Add(VWindow.Background);
            VMethods.Add(VWindow.WindowTab);*/

            List<Button> buttons = new List<Button>();
            for(int i=0; i< FuncNames.Length; i++)
            {
                Button newButton = new Button
                {
                    Width = 100,
                    Location = new Point(200, 0),
                    Text = FuncNames[i]

                };
                newButton.Click += delegate (object sender,EventArgs e)
                {
                    Thread.Sleep(500);
                    VMethods[(int)newButton.Location.Y/30]();
                };
                buttons.Add(newButton);
                this.Controls.Add(newButton);
            }
            FormClosing += OnClosing;
        }
        
        protected virtual void OnClosing(Object sender, CancelEventArgs e)
        {
            if(USBThread != null)
            {
                USBThread.Abort();
            }
            if(BTThread != null)
            {
                BTThread.Abort();
            }
            if(VWindow.GCDesktop != null)
            {
                VWindow.GCDesktop.Abort();
            }

            Application.Exit();
            Environment.Exit(Environment.ExitCode);
        }

        void HideForm()
        {
            this.Visible = false;
            this.ShowInTaskbar = false;
        }

        void main()
        {
            /*
            bool _isSupportedInternal = true;

            bool supportedInternal = Environment.OSVersion.Version.Major >= 10 && _isSupportedInternal;
            Console.WriteLine(Environment.OSVersion.Version.Major);
            Console.WriteLine(VirtualDesktop._isSupportedInternal);
            Console.WriteLine(Environment.OSVersion.Version.Major);
            Dispatcher.CurrentDispatcher.Invoke(
                new Action
                (
                    delegate
                    {
                        var desktops = VirtualDesktop.GetDesktops();
                        foreach (var desktop in desktops)
                        {
                            Console.WriteLine(desktop);
                        }
                    }
                )
                ,
                DispatcherPriority.Normal
            );
            var desktops = VirtualDesktop.GetDesktops();
            foreach (var desktop in desktops)
            {
                Console.WriteLine(desktop);
            }
            Console.WriteLine(desktops);
            */
        }

    }
}
