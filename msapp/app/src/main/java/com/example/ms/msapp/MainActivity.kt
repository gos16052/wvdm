@file:JvmName("MainActivity")
package com.example.ms.msapp


import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.text.method.Touch
import android.util.Log
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    var comm: Communicator? = null
    var touchMgr: TouchManager ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.mainLayout.setOnTouchListener(TouchListener(this))

        // USB 연결요청
        comm = UsbCommunicator(this, this.applicationContext) as Communicator
        if(!comm!!.conn()) { // USB 실패하면 BT로 연결
            Log.d("USB", "FAILED")
            var BTIntent:Intent = Intent(applicationContext, BTActivity::class.java)
            startActivity(BTIntent)
        }

    }

    override fun onStop() {
        if(BTSingleton.clientThread != null) {
            BTSingleton.clientThread.cancel()
            BTSingleton.clientThread.interrupt()
            BTSingleton.clientThread = null
        }
        if(BTSingleton.socketThread != null) {
            BTSingleton.socketThread.interrupt()
            BTSingleton.socketThread = null
        }
        super.onStop()
    }
    override fun onDestroy() {
        if(BTSingleton.clientThread != null) {
            BTSingleton.clientThread.cancel()
            BTSingleton.clientThread.interrupt()
            BTSingleton.clientThread = null
        }
        if(BTSingleton.socketThread != null) {
            BTSingleton.socketThread.interrupt()
            BTSingleton.socketThread = null
        }
        super.onDestroy()
    }

    inner class TouchListener(ctx: Context) : View.OnTouchListener {
        override fun onTouch(v: View?, e: MotionEvent?): Boolean {
            if (e == null) {
                Log.d("touch", "null")
                return false
            }
            if (touchMgr == null){
                touchMgr = TouchManager(BTSingleton.socketThread)
            }

            //Log.d("touch", "called" )


            touchMgr!!.touch(v,e)


            if(e.action == MotionEvent.ACTION_UP) {
                return false
            }
            return true
        }

    }

}


