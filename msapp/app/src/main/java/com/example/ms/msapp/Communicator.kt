package com.example.ms.msapp

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.*
import android.util.Log
import android.widget.ListView
import java.util.*


/**
 * Created by MS on 2017-10-08.
 */
abstract class Communicator(ac: Activity, c: Context) {
    var ac: Activity = ac
    var c: Context = c

    abstract fun conn(): Boolean
    abstract fun send(buf: ByteArray): Int
    abstract fun recv(buf: ByteArray): Int
}


/* http://dsnight.tistory.com/36 참조 */
/*
class BluetoothCommunicator(ac: Activity, c: Context): Communicator(ac, c) {
    /* constants */
    val REQUEST_CONNECT_DEVICE = 1
    val REQUEST_ENABLE_BT = 2
    val BLUETOOTH_NAME = "BluetoothEx"
    val BLUETOOTH_UUID: UUID = UUID.fromString("test")

    var TAG: String = "BluetoothService"
    var btAdaptor: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    var btReceiver: BroadcastReceiver = BTReceiver()
    var remoteDevice: ArrayList<String> = ArrayList<String>()
    var cThread: ClientThread? = null // 클라이언트 소켓 접속 쓰레드
    var sThread: ServerThread? = null // 서버 소켓 접속 쓰레드
    var socketThread: SocketThread? = null // 데이터 송수신 쓰레드
    var listDevice: ListView = findViewById
    inner class BTReceiver():BroadcastReceiver() {
        //원격디바이스검색이벤트수신
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent == null) {
                Log.d("BT", "ONRECEIVE, Intent is null")
                return
            }
            if(intent.action == BluetoothDevice.ACTION_FOUND){
                //인텐트에서 디바이스 정보 추출
                var device: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                //페어링된 디바이스가 아니라면
                if(device.bondState != BluetoothDevice.BOND_BONDED){
                    //디바이스를 목록에 추가
                    addDeviceToList(device.name, device.address)
                }
            }
        }
    }

    //디바이스를 list view에 추가
    fun addDeviceToList(name: String, address: String){
        //listview와 연결된 arraylist에 새 항목을 추가
        var deviceInfo: String = "$name-$address"
        Log.d("BT", "DEVICE IS FOUND $deviceInfo")
        remoteDevice.add(deviceInfo)
        //화면 갱신

        var adapter: ArrayAdapter
    }


    // 원격 디바이스 검색 시작
    fun startFindingDevice() {
        //원격 디바이스 검색 중지
        stopFindingDevice()
        //디바이스 검색 시작
        btAdaptor.startDiscovery()
        //원격디바이스 검색 이벤트 리시버 등록
        registerReceiver(mbtReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
    }

    fun getParedDevice(){

    }

    override fun conn(): Boolean{
        if(btAdaptor == null){
            Log.d("BT", "Bluetoothe is not available")
            return false
        }
        if(!btAdaptor.isEnabled){
            Log.d("BT", "Bluetooth is not enabled")
            var intent: Intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            ac.startActivityForResult(intent, REQUEST_ENABLE_BT)

        }
        return true
    }

    override fun send(buf: ByteArray): Int {
        return 0
    }

    override fun recv(buf: ByteArray): Int {
        return 0
    }

}
*/
/*
http://devguru.co.kr/blog/5851/
https://developer.android.com/reference/android/hardware/usb/UsbDevice.html
https://developer.android.com/guide/topics/connectivity/usb/host.html
https://developer.android.com/guide/topics/connectivity/usb/host.html

* */
class UsbCommunicator(ac: Activity, c: Context): Communicator(ac, c) {
    /* constants */
    var VID: Int =  0x8086
    var PID: Int =  0xA12F
    var TIMEOUT: Int = 1
    var LENGTH: Int = 32

    var mUsbManager: UsbManager = c.getSystemService(Context.USB_SERVICE) as UsbManager
    var mUsbDeviceConnection: UsbDeviceConnection? = null
    var mUsbDevice: UsbDevice? = null
    var epIn: UsbEndpoint? = null
    var epOut: UsbEndpoint? = null

    override fun conn(): Boolean {
        mUsbDevice = getDevice()
        if(mUsbDevice == null){
            Log.d("USB", "mDevice is null")
            return false
        }

        var usbInterface: UsbInterface = (mUsbDevice as UsbDevice).getInterface(0)
        var conn: UsbDeviceConnection = mUsbManager.openDevice(mUsbDevice)
        if(conn == null) {
            Log.d("USB", "usb interface is null")
            return false
        }
        mUsbDeviceConnection = conn
        if(usbInterface == null){
            Log.d("USB", "usbInterface is null")
            return false
        }
        if(!conn.claimInterface(usbInterface,true )){
            Log.d("USB", "claimInterface is failed")
            return false
        }

        (0..usbInterface.endpointCount).forEach { i ->
            var endPoint:UsbEndpoint = usbInterface.getEndpoint(i)
            if(endPoint.type == UsbConstants.USB_ENDPOINT_XFER_BULK){
                if(endPoint.direction == UsbConstants.USB_DIR_IN){
                    this.epIn = endPoint
                } else {
                    this.epOut = endPoint
                }
            }
        }
        return true
    }

    fun getDevice(): UsbDevice? {
        var devlist: HashMap<String, UsbDevice> = mUsbManager.deviceList
        var devIter: Iterator<UsbDevice> = devlist.values.iterator()
        var size:Int = devlist.size
        Log.d("USB", "devlistSize:$size")
        while (devIter.hasNext()){
            var d: UsbDevice = devIter.next()
            if(d.vendorId == VID && d.productId == PID){
                Log.d("USB", "VENDOR:$VID PRODUCT:$PID")
                return d
            }
        }
        return null
    }

    override fun send(buf: ByteArray): Int {
        return mUsbDeviceConnection!!.bulkTransfer(epOut, buf, LENGTH, TIMEOUT)
    }

    override fun recv(buf: ByteArray): Int {
        return mUsbDeviceConnection!!.bulkTransfer(epIn, buf, LENGTH, TIMEOUT)
    }
}