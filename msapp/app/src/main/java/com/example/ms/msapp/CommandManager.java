package com.example.ms.msapp;

import java.nio.ByteBuffer;

/**
 * Created by MS on 2017-10-10.
 */

/* big endian to little endian */
public class CommandManager {
    private final int SIZE_SCHEME = 32;
    private final int SIZE_CMD = 1;
    private final int SIZE_BYTE = 1;
    private final int SIZE_INT = 4;
    private final int SIZE_FLOAT = 4;
    private final int SIZE_DOUBLE = 4;

    private byte[] toCmd(int command){
        byte[] b = new byte[SIZE_SCHEME]; // init by zeros
        b[0] = (byte)command;
        return b;
    }

    private byte[] fillZero(byte[] b, int index){
        for(int i = index; i< SIZE_SCHEME; i++){
            b[i]=0;
        }
        return b;
    }

    private byte[] toExtend(byte[] b, int index, byte value){
        b[index] = value;
        return b;
    }

    // return index
    private byte[] toExtend(byte[] b, int index, int value){
        byte[] intArr = ByteBuffer.allocate(SIZE_INT).putInt(value).array();
        for(int i=0; i<SIZE_INT; i++){
            b[index + i] = intArr[i];
        }
        return b;
    }


    private byte[] cmdEmpty(int cmd){
        int index = SIZE_CMD;
        byte[] b = toCmd(cmd);

        b = fillZero(b, index);
        BTSingleton.socketThread.write(b);
        return b;
    }

    private byte[] cmdByte(int cmd, byte a){
        int index = SIZE_CMD;
        byte[] b = toCmd(cmd);

        b = toExtend(b, index, a);
        index += SIZE_BYTE;

        b = fillZero(b, index);
        BTSingleton.socketThread.write(b);
        return b;
    }

    private byte[] cmdIntInt(int cmd, int x, int y){
        int index = SIZE_CMD;
        byte[] b = toCmd(cmd);
        b = toExtend(b, index, x);
        index += SIZE_INT;
        b = toExtend(b, index, y);
        index += SIZE_INT;

        b = fillZero(b, index);
        BTSingleton.socketThread.write(b);
        return b;
    }

    public byte[] cmdClick(){
//        byte[] b = new byte[32];
//        for(int i=0; i<32; i++){
//            b[i]=1;
//        }
//        return b;
        return cmdEmpty(10);
    }

    public byte[] cmdMove(int dx, int dy){ return cmdIntInt(11, dx, dy); }

    public byte[] cmdTouchDown(){ return cmdEmpty(12); }
    public byte[] cmdTouchUp(){ return cmdEmpty(13); }

    public byte[] cmdRightClick(){
        return cmdEmpty(21);
    }

    public byte[] cmdViewControl(int dx, int dy){
        return cmdIntInt(22, dx, dy);
    }

    public byte[] cmdPageControl(int direction){
        return cmdByte(23, (byte)direction);
    }

    public byte[] cmdProgramList(int direction){
        return cmdByte(31, (byte)direction);
    }

    public byte[] cmdChangeWindow(int direction){
        return cmdByte(32, (byte)direction);
    }

    public byte[] cmdBackground(){
        return cmdEmpty(41);
    }

    public byte[] cmdFullScreen(){
        return cmdEmpty(42);
    }




    /*
        byte[] b = new byte[32]; // init by zero

        return b;
     */
}

/*
    명령어(1B) | extend(31B)

    명령어정의
    ------------------------------------------------------------
    | cmd | extend             | #touch | operation
    |-----|--------------------|--------|-----------------------
                                                                                        |  1  |                    | 0      | 해상도 요청
    | 10  |                    | 1      | 클릭
    | 11  | dx(4B) dy(4B)      | 1      | 이동
    | 12  |                    | 1      | 터치다운
    | 13  |                    | 1      | 터치업
    | 21  |                    | 2      | 오른쪽클릭
    | 22  | dx(4B) dy(4B)      | 2      | 페이지보기 위치이동(2터치 상하좌우)          viewcontrol
    | 23  | LEFT:1,RIGHT:2(1B) | 2      | 이전페이지/다음페이지
    | 31  | UP:1,DOWN:2(1B)    | 3      | 프로그램리스트 ON/OFF
    | 32  | LEFT:1,RIGHT:2(1B) | 3      | 윈도우전환
    | 41  |                    | 4-5    | 바탕화면
    | 42  |                    | 4-5    | 작업관리자(?)
    ------------------------------------------------------------


 */



/*

    private int toExtend(byte[] b, int index, float value){
        byte[] floatArr = ByteBuffer.allocate(SIZE_FLOAT).putFloat(value).array();
        for(int i=0; i<SIZE_FLOAT; i++){
            b[index + i] = floatArr[i];
        }
        return index + SIZE_FLOAT;
    }

    public byte[] cmdClick(){
        int index = SIZE_CMD; // "cmd" occupies 1 byte
        byte[] b = toCmd(10);
        return b;
    }

    public byte[] cmdMove(double dx, double dy){
        int index = SIZE_CMD;
        byte[] b = toCmd(11);
        index = toExtend(b, index, dx);
        index = toExtend(b, index, dy);
        return b;
    }

    public byte[] cmdRightClick(){
        int index = SIZE_CMD;
        byte[] b = toCmd(21);
        return b;
    }

    public byte[] cmdViewControl(double dx, double dy){
        int index = SIZE_CMD;
        byte[] b = toCmd(22);
        index = toExtend(b, index, dx);
        index = toExtend(b, index, dy);
        return b;
    }

    public byte[] cmdPageControl(int direction){
        int index = SIZE_CMD;
        byte[] b = toCmd(23);
        index = toExtend(b, index, direction);
        return b;
    }

    public byte[] cmdProgramList(int direction){
        int index = SIZE_CMD;
        byte[] b = toCmd(31);
        index = toExtend(b, index, direction);
        return b;
    }

    public byte[] cmdChangeWindow(int direction){
        int index = SIZE_CMD;
        byte[] b = toCmd(32);
        index = toExtend(b, index, direction);
        return b;
    }

    public byte[] cmdBackground(){
        int index = SIZE_CMD;
        byte[] b = toCmd(41);
        return b;
    }

    public byte[] cmdTaskManager(){
        int index = SIZE_CMD;
        byte[] b = toCmd(42);
        return b;
    }

 */