package com.example.ms.msapp

import android.content.Context
import android.os.SystemClock.sleep
import android.text.method.Touch
import android.util.Log
import android.view.MotionEvent
import android.view.View

/**
 * Created by MS on 2017-10-07.
 */

class TouchManager (socket: BTSingleton.SocketThread) {
    /* constants */
    val DIR_UP =        1
    val DIR_RIGHT =     2
    val DIR_DOWN =      3
    val DIR_LEFT =      4
    /* constants end */

    /* the number of touches */
    var nr_touch: Int = 0

    /* previous coordinates up to 4 points */
    var org_x: Array<Float> = arrayOf(0.toFloat(),0.toFloat(),0.toFloat(),0.toFloat())
    var org_y: Array<Float> = arrayOf(0.toFloat(),0.toFloat(),0.toFloat(),0.toFloat())

    /* entry points */
    var ent_x: Array<Float> = arrayOf(0.toFloat(),0.toFloat(),0.toFloat(),0.toFloat())
    var ent_y: Array<Float> = arrayOf(0.toFloat(),0.toFloat(),0.toFloat(),0.toFloat())

    /* current coordinates up to 4 points */
    var new_x: Array<Float> = arrayOf(0.toFloat(),0.toFloat(),0.toFloat(),0.toFloat())
    var new_y: Array<Float> = arrayOf(0.toFloat(),0.toFloat(),0.toFloat(),0.toFloat())

    /* ACTION_MOVE에서 움직여라, 뷰이동해라 호출되었으면 ACTION_UP에서 추가적인 커맨드를 보낼 필요 없음*/
    var moved_count: Int = 0
    var touchdowned = false
    var moved_up = false

    var cmdMgr: CommandManager = CommandManager()
    var socket: BTSingleton.SocketThread = socket;

    fun init(){
        moved_count = 0
        touchdowned = false
        moved_up = false

        org_x[0]=0.toFloat()
        org_x[1]=0.toFloat()
        org_x[2]=0.toFloat()
        org_x[3]=0.toFloat()
        org_y[0]=0.toFloat()
        org_y[1]=0.toFloat()
        org_y[2]=0.toFloat()
        org_y[3]=0.toFloat()

        ent_x[0]=0.toFloat()
        ent_x[1]=0.toFloat()
        ent_x[2]=0.toFloat()
        ent_x[3]=0.toFloat()
        ent_y[0]=0.toFloat()
        ent_y[1]=0.toFloat()
        ent_y[2]=0.toFloat()
        ent_y[3]=0.toFloat()

        new_x[0]=0.toFloat()
        new_x[1]=0.toFloat()
        new_x[2]=0.toFloat()
        new_x[3]=0.toFloat()
        new_y[0]=0.toFloat()
        new_y[1]=0.toFloat()
        new_y[2]=0.toFloat()
        new_y[3]=0.toFloat()
    }

    /* 시계방향 */
    var ALIGN_90 = 1
    var ALIGN_180 = 2
    var ALIGN_270 = 3

    var align = 3
    fun getEx(e: MotionEvent, index:Int): Float {
        when(align){
            1-> return -e.getY(index)
            2-> return -e.getX(index)
            3-> return e.getY(index)
        }
        return e.getX(index)
    }

    fun getEy(e: MotionEvent, index:Int): Float{
        when(align){
            1-> return e.getX(index)
            2-> return -e.getY(index)
            3-> return -e.getX(index)
        }
        return e.getY(index)
    }

    fun updateEntry(index:Int, x:Float, y:Float) {
        ent_x[index] = x
        ent_y[index] = y
    }

    // index번째만 업데이트 됨
    fun updateCoord(index:Int, x:Float, y:Float){
        org_x[index] = new_x[index]
        org_y[index] = new_y[index]
        new_x[index] = x
        new_y[index] = y
    }


    fun getDelta(new:Float, org:Float): Float{
        return new - org
    }

    /* must be called after updateCoord*/
    fun getDirection(): Int{
        var avg_dx:Float = 0.toFloat()
        var avg_dy:Float = 0.toFloat()
        for(i in 0..(nr_touch-1)){
            avg_dx += getDelta(new_x[i], ent_x[i])
            avg_dy += getDelta(new_y[i], ent_y[i])
        }

        /* dx의 평균을 구함 */
        avg_dx /= nr_touch
        avg_dy /= nr_touch
        //Log.d("getDirection", "$avg_dx $avg_dy")
        if(Math.abs(avg_dx) < Math.abs(avg_dy)){
            if(avg_dy> 0){
                return DIR_DOWN
            } else {
                return DIR_UP
            }
        } else {
            if(avg_dx > 0){
                return DIR_RIGHT
            } else {
                return DIR_LEFT
            }
        }
    }

    fun isCentralized(): Boolean{
        var avg_ent_x: Float = 0.toFloat()
        var avg_ent_y: Float = 0.toFloat()
        var avg_new_x: Float = 0.toFloat()
        var avg_new_y: Float = 0.toFloat()

        for(i in 0..(nr_touch-1)){
            avg_ent_x += ent_x[i]
            avg_ent_y += ent_y[i]
            avg_new_x += new_x[i]
            avg_new_y += new_y[i]
        }
        avg_ent_x /= nr_touch
        avg_ent_y /= nr_touch
        avg_new_x /= nr_touch
        avg_new_y /= nr_touch

        /* 거리 구함 */
        var sum_dist_ent_x: Float = 0.toFloat()
        var sum_dist_ent_y: Float = 0.toFloat()
        var sum_dist_new_x: Float = 0.toFloat()
        var sum_dist_new_y: Float = 0.toFloat()

        for(i in 0..(nr_touch-1)) {
            sum_dist_ent_x += avg_ent_x - ent_x[i]
            sum_dist_ent_y += avg_ent_y - ent_y[i]
            sum_dist_new_x += avg_new_x - new_x[i]
            sum_dist_new_y += avg_new_y - new_y[i]
        }

        /* 모두 제곱시킴 */
        sum_dist_ent_x = Math.pow(sum_dist_ent_x.toDouble(),2.toDouble()).toFloat()
        sum_dist_ent_y = Math.pow(sum_dist_ent_y.toDouble(),2.toDouble()).toFloat()
        sum_dist_new_x = Math.pow(sum_dist_new_x.toDouble(),2.toDouble()).toFloat()
        sum_dist_new_y = Math.pow(sum_dist_new_y.toDouble(),2.toDouble()).toFloat()

        var dist_ent = Math.pow((sum_dist_ent_x + sum_dist_ent_y).toDouble(), 0.5.toDouble()).toFloat()
        var dist_new = Math.pow((sum_dist_new_x + sum_dist_new_y).toDouble(), 0.5.toDouble()).toFloat()
        Log.d("4touch", "$dist_ent   $dist_new")
        if(dist_new < dist_ent){
            return true
        }
        return false
    }

    fun msFloor(v: Float): Float{
        if(v < 1) return 0.toFloat()
        return v
    }

    /*
       action_down과 action_up은 모든 갯수의 터치에서 호출됨

       2개 터치일때 아래와 같은 순서로 호출됨
       D/touchCheck: ViewRoot's Touch Event : MotionEvent { action=ACTION_DOWN, id[0]=0, x[0]=546.24133, y[0]=664.1638, toolType[0]=TOOL_TYPE_FINGER, buttonState=0, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=1, historySize=0, eventTime=11543728, downTime=11543728, deviceId=8, source=0x1002 }
       D/touchCheck: ViewRoot's Touch Event : MotionEvent { action=ACTION_POINTER_DOWN(1), id[0]=0, x[0]=546.24133, y[0]=664.1638, toolType[0]=TOOL_TYPE_FINGER, id[1]=1, x[1]=298.5853, y[1]=687.1459, toolType[1]=TOOL_TYPE_FINGER, buttonState=0, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=2, historySize=0, eventTime=11543728, downTime=11543728, deviceId=8, source=0x1002 }
       D/tag1: ACTION_MOVE
       D/touchCheck: ViewRoot's Touch Event : MotionEvent { action=ACTION_POINTER_UP(1), id[0]=0, x[0]=547.39594, y[0]=664.1638, toolType[0]=TOOL_TYPE_FINGER, id[1]=1, x[1]=298.5853, y[1]=687.1459, toolType[1]=TOOL_TYPE_FINGER, buttonState=0, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=2, historySize=0, eventTime=11543818, downTime=11543728, deviceId=8, source=0x1002 }
       D/tag1: ACTION_MOVE
       D/touchCheck: ViewRoot's Touch Event : MotionEvent { action=ACTION_UP, id[0]=0, x[0]=548.3617, y[0]=665.2862, toolType[0]=TOOL_TYPE_FINGER, buttonState=0, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=1, historySize=0, eventTime=11543830, downTime=11543728, deviceId=8, source=0x1002 }

       ACTION_DOWN: 1개터치가 눌려졌음
       ACTION_POINTER_DOWN: 현재 눌려진 터치에 1개터치가 추가됨(총 2개)
       ACTION_MOVE: 2개가 눌린상태에서 무브가 호출됨 pointerCount=2임
       ACTION_POINTER_UP: 현재 눌려진 터치에 1개터치가 떼짐
       ACTION_MOVE: 1개가 눌린상태에서 무브가 호출됨, 이부분 무시 필요
       ACTION_UP: 모든터치가 뗴짐

     */
    fun touch(v: View?, e: MotionEvent){
        //Log.d("touch", "called ${e.pointerCount} ${e.action}")
        /* 각 포인터 카운트/ 액션/ nr_touch에 따라 행동이달라짐 */
        when(e.pointerCount){
            1->{
                updateCoord(0, getEx(e, 0), getEy(e,0))
                when(e.action){
                    MotionEvent.ACTION_DOWN->{
                        /* 모든 터치의 시작지점 */
                        nr_touch = 1
                        init()
                        updateCoord(0, getEx(e, 0), getEy(e,0))
                        updateEntry(0, getEx(e,0), getEy(e,0))
                        Thread({
                            sleep(1000);
                            if(moved_count == 0) {
                                cmdMgr.cmdTouchDown()
                                touchdowned = true
                            }
                        }).start()
                    }
                    MotionEvent.ACTION_MOVE->{
                        if(moved_up) {
                            moved_up=false
                            updateCoord(0,getEx(e,0),getEy(e,0))
                            updateCoord(0,getEx(e,0),getEy(e,0))
                            return
                        }
                        if(nr_touch <= 2){
                            //Log.d("move", "e $getEx(e) $getEy(e)")
                            var dx = getDelta(new_x[0], org_x[0]).toInt()
                            var dy = getDelta(new_y[0], org_y[0]).toInt()
                            //Log.d("tag1", "move $dx $dy")

                            if(dx == 2 && dy < 2) return;
                            // dx dy 둘중 하나라도 1이상이어야 move
                            moved_count += 1
                            Log.d("cmd","move $dx $dy")
                            cmdMgr.cmdMove(dx, dy)

                        }
                    }
                    MotionEvent.ACTION_UP->{
                        when(nr_touch){
                            1->{
                                if(moved_count <= 1){
                                    Log.d("cmd","click")
                                    cmdMgr.cmdClick()
                                    
                                }
                                if(touchdowned){
                                    cmdMgr.cmdTouchUp()
                                    touchdowned = false
                                }
                            }
                            2->{
                                if(touchdowned){
                                    cmdMgr.cmdTouchUp()
                                    touchdowned = false
                                    return
                                }
                                if(moved_count <= 1){

                                    var dx1 = getDelta(new_x[0], ent_x[0])
                                    var dx2 = getDelta(new_x[0], ent_x[1])
                                    var dx3 = getDelta(new_x[1], ent_x[0])
                                    var dx4 = getDelta(new_x[1], ent_x[1])

                                    var dx_self = dx1 + dx4
                                    var dx_chg = dx2 + dx3
                                    var dx: Float
                                    if(dx_self < dx_chg){ dx = dx_self }
                                    else { dx = dx_chg }

                                    Log.d("dx", "" +new_x[0]+" "+ent_x[0])

                                    if(Math.abs(dx) < 10){
                                            //||Math.abs(new_x[1] - ent_x[1])<10 || Math.abs(new_x[1] - ent_x[0]) < 10){ // 안드로이드 버그 ㅠㅠ
                                        /* 안드로이드 버그..
D/touchCheck: ViewRoot's Touch Event : MotionEvent { action=ACTION_DOWN, id[0]=0, x[0]=222.69072, y[0]=774.07794, toolType[0]=TOOL_TYPE_FINGER, buttonState=0, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=1, historySize=0, eventTime=19477017, downTime=19477017, deviceId=8, source=0x1002 }
D/touchCheck: ViewRoot's Touch Event : MotionEvent { action=ACTION_POINTER_DOWN(1), id[0]=0, x[0]=222.69072, y[0]=774.07794, toolType[0]=TOOL_TYPE_FINGER, id[1]=1, x[1]=457.36478, y[1]=837.02875, toolType[1]=TOOL_TYPE_FINGER, buttonState=0, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=2, historySize=0, eventTime=19477017, downTime=19477017, deviceId=8, source=0x1002 }
D/touchCheck: ViewRoot's Touch Event : MotionEvent { action=ACTION_POINTER_UP(0), id[0]=0, x[0]=223.68933, y[0]=775.07715, toolType[0]=TOOL_TYPE_FINGER, id[1]=1, x[1]=456.36618, y[1]=837.02875, toolType[1]=TOOL_TYPE_FINGER, buttonState=0, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=2, historySize=0, eventTime=19477068, downTime=19477017, deviceId=8, source=0x1002 }
D/touchCheck: ViewRoot's Touch Event : MotionEvent { action=ACTION_UP, id[0]=1, x[0]=456.36618, y[0]=837.02875, toolType[0]=TOOL_TYPE_FINGER, buttonState=0, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=1, historySize=0, eventTime=19477068, downTime=19477017, deviceId=8, source=0x1002 }
                                           좌표 이상한인식
                                         */
                                        Log.d("cmd","rightclick")
                                        cmdMgr.cmdRightClick()
                                        
                                    } else {
                                        var direction:Int
                                        if(dx > 0){
                                            direction = DIR_RIGHT
                                        } else {
                                            direction = DIR_LEFT
                                        }
                                        Log.d("cmd", "pagecontrol $direction")
                                        cmdMgr.cmdPageControl(direction)
                                    }
                                }
                            }
                            3->{
                                var dir = getDirection()
                                var cmd:ByteArray
                                if(dir == DIR_DOWN || dir == DIR_UP){
                                    Log.d("cmd","programlist $dir")
                                    cmdMgr.cmdProgramList(dir)
                                } else {
                                    Log.d("cmd","changewindow $dir")
                                    cmdMgr.cmdChangeWindow(dir)
                                }
                                
                            }
                            4->{
                                var cmd:ByteArray
                                if(isCentralized()){
                                    Log.d("cmd","background")
                                    cmdMgr.cmdBackground()
                                } else {
                                    Log.d("cmd","fullscreen")
                                    cmdMgr.cmdFullScreen()
                                }
                                
                            }
                        }
                    }
                }
            }
            2->{

                when(e.action){
                    MotionEvent.ACTION_POINTER_2_DOWN->{
                        moved_count++;
                        updateCoord(1, getEx(e,1), getEy(e,1))
                        if(nr_touch < 2) nr_touch = 2
                        updateEntry(1, getEx(e,1), getEy(e,1))
                    }
                    MotionEvent.ACTION_POINTER_UP->{
                        if(nr_touch == 2){
                            moved_up = true
                        }
                        updateCoord(0, getEx(e, e.actionIndex), getEy(e,e.actionIndex))
                    }
                    MotionEvent.ACTION_POINTER_1_UP->{
                        if(nr_touch == 2){
                            moved_up = true
                        }
                        updateCoord(0, getEx(e, e.actionIndex), getEy(e,e.actionIndex))
                    }
                    MotionEvent.ACTION_POINTER_2_UP->{
                        updateCoord(1, getEx(e,e.actionIndex), getEy(e,e.actionIndex))
                    }
                    MotionEvent.ACTION_POINTER_3_UP->{
                        updateCoord(2, getEx(e,e.actionIndex), getEy(e, e.actionIndex))
                    }
                    MotionEvent.ACTION_MOVE->{
                        /*
                        if(nr_touch == 2){

                            var dx = getDelta(new_x[0], org_x[0])
                            var dy = getDelta(new_y[0], org_y[0])
                            var duration = e.eventTime - e.downTime
                            if(duration > 100){
                                Log.d("2Move", "viewcontrol")
                                moved_count += 2 // 100미리넘었으니까
                                var cmd = cmdMgr.cmdViewControl(dx,dy)
                                
                            }
                        }
                        */
                    }
                }
            }
            3->{
                updateCoord(2, getEx(e,2), getEy(e,2))
                when(e.action){
                    MotionEvent.ACTION_POINTER_3_DOWN->{
                        moved_count++;
                        if(nr_touch < 3) nr_touch = 3
                        updateEntry(2, getEx(e,2), getEy(e,2))
                    }
                    MotionEvent.ACTION_POINTER_1_UP->{
                        updateCoord(0, getEx(e,e.actionIndex), getEy(e,e.actionIndex))
                    }
                    MotionEvent.ACTION_POINTER_2_UP->{
                        updateCoord(1, getEx(e,e.actionIndex), getEy(e,e.actionIndex))
                    }
                    MotionEvent.ACTION_POINTER_3_UP->{
                        updateCoord(2, getEx(e,e.actionIndex), getEy(e, e.actionIndex))
                    }
                }
            }
            4->{
                updateCoord(3, getEx(e,3), getEy(e,3))
                when(e.action){
                    773->{ // pointer_4_down
                        moved_count++;
                        nr_touch = 4
                        updateEntry(3, getEx(e,3), getEy(e,3))
                    }
                    MotionEvent.ACTION_POINTER_1_UP->{
                        updateCoord(0, getEx(e,e.actionIndex), getEy(e,e.actionIndex))
                    }
                    MotionEvent.ACTION_POINTER_2_UP->{
                        updateCoord(1, getEx(e,e.actionIndex), getEy(e,e.actionIndex))
                    }
                    MotionEvent.ACTION_POINTER_3_UP->{
                        updateCoord(2, getEx(e,e.actionIndex), getEy(e, e.actionIndex))
                    }
                }
            }
        }

        //Log.d("touch", " " + new_x[0] + " " + new_y[0] + " " + org_x[0] + " " + org_y[0])
        //Log.d("touch", " " + new_x[1] + " " + new_y[1] + " " + org_x[1] + " " + org_y[1])
    }
}
