package com.example.ms.msapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Created by MS on 2017-10-16.
 */

public class BTSingleton {
    public static ClientThread clientThread = null;
    //public static ServerThread serverThread = null;
    public static SocketThread socketThread = null;
    private static BluetoothAdapter mBA = null;

    public static BluetoothAdapter getBluetoothAdapter(){
        if(mBA == null){
            mBA = BluetoothAdapter.getDefaultAdapter();
        }
        return mBA;
    }
    //init할때만 device 너어줍니당
    public static ClientThread getClientThread(BluetoothDevice device){
        if(clientThread == null){
            clientThread = new ClientThread(device);
        }
        return clientThread;
    }
/*
    public static ServerThread getServerThread(){
        if(serverThread == null){
            serverThread = new ServerThread();
        }
        return serverThread;
    }
*/
    public static SocketThread getSocketThread(BluetoothSocket socket){
        if(socketThread == null){
            socketThread = new SocketThread(socket);
        }
        return socketThread;
    }


    public static BTActivity btActivity;
    // 원격 디바이스와 접속되었으면 데이터 송수신 스레드를 시작
    public static void onConnected(BluetoothSocket socket) {
        showMessage("Socket connected");

        // 데이터 송수신 스레드가 생성되어 있다면 삭제한다
        if( socketThread != null )
            socketThread = null;
        // 데이터 송수신 스레드를 시작
        socketThread = BTSingleton.getSocketThread(socket);
        socketThread.start();

        btActivity.finish();
    }

    public static void showMessage(String strMsg) {
        Log.d("tag1", strMsg);
    }

    // 클라이언트 소켓 생성을 위한 스레드
    public static class ClientThread extends Thread {
        private BluetoothSocket mmCSocket;

        // 원격 디바이스와 접속을 위한 클라이언트 소켓 생성
        public ClientThread(BluetoothDevice device) {
            try {
                //mskim
                mBA.cancelDiscovery();
                boolean temp = device.fetchUuidsWithSdp();
                UUID muuid = null;
                if(temp){
                    muuid = device.getUuids()[0].getUuid();
                    showMessage("uuid is changed to " + muuid);
                } else {
                    muuid = BTActivity.BLUE_UUID;
                }
                //mskim

                //mskim
                // https://stackoverflow.com/questions/11428820/bluetoothserversocket-accept-fails-and-throws-an-ioexception/11878181#11878181
                try {
                    Method m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
                    mmCSocket = (BluetoothSocket) m.invoke(device, Integer.valueOf(1));
                } catch (NoSuchMethodException e) {
                    showMessage("NoSuchMethodException");
                } catch (IllegalAccessException e) {
                    showMessage("IllegalAccessException");
                } catch (InvocationTargetException e) {
                    showMessage("InvocationTargetException");
                }

                //mmCSocket = device.createInsecureRfcommSocketToServiceRecord(muuid);
            } catch(Exception e){//IOException e) {
                showMessage("Create Client Socket error");
                return;
            }
        }

        public void run() {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 원격 디바이스와 접속 시도
            try {
                mmCSocket.connect();
            } catch(IOException e) {
                showMessage("Connect to server error"+e.getMessage());
                // 접속이 실패했으면 소켓을 닫는다
                try {
                    mmCSocket.close();
                } catch (IOException e2) {
                    showMessage("Client Socket close error");
                }
                return;
            }

            // 원격 디바이스와 접속되었으면 데이터 송수신 스레드를 시작
            onConnected(mmCSocket);
        }

        // 클라이언트 소켓 중지
        public void cancel() {
            try {
                mmCSocket.close();
            } catch (IOException e) {
                showMessage("Client Socket close error");
            }
        }
    }
/*
    // 서버 소켓을 생성해서 접속이 들어오면 클라이언트 소켓을 생성하는 스레드
    public static class ServerThread extends Thread {
        private BluetoothServerSocket mmSSocket;

        // 서버 소켓 생성
        public ServerThread() {
            try {
                mmSSocket = mBA.listenUsingInsecureRfcommWithServiceRecord(BTActivity.BLUE_NAME, BTActivity.BLUE_UUID);
            } catch(IOException e) {
                showMessage("Get Server Socket Error");
            }
        }

        public void run() {
            BluetoothSocket cSocket = null;

            // 원격 디바이스에서 접속을 요청할 때까지 기다린다
            try {
                cSocket = mmSSocket.accept();
            } catch(IOException e) {
                showMessage("Socket Accept Error");
                return;
            }

            // 원격 디바이스와 접속되었으면 데이터 송수신 스레드를 시작
            onConnected(cSocket);
        }

        // 서버 소켓 중지
        public void cancel() {
            try {
                mmSSocket.close();
            } catch (IOException e) {
                showMessage("Server Socket close error");
            }
        }
    }
*/
    // 데이터 송수신 스레드
    public static class SocketThread extends Thread {
        private final BluetoothSocket mmSocket; // 클라이언트 소켓
        private InputStream mmInStream; // 입력 스트림
        private OutputStream mmOutStream; // 출력 스트림

        public SocketThread(BluetoothSocket socket) {
            mmSocket = socket;

            // 입력 스트림과 출력 스트림을 구한다
            try {
                //mmInStream = socket.getInputStream();
                mmOutStream = socket.getOutputStream();
            } catch (IOException e) {
                showMessage("Get Stream error");
            }
        }
/*
        // 소켓에서 수신된 데이터를 화면에 표시한다
        public void run() {
            byte[] buffer = new byte[1024];
            int bytes;

            while (true) {
                try {
                    // 입력 스트림에서 데이터를 읽는다
                    bytes = mmInStream.read(buffer);
                    String strBuf = new String(buffer, 0, bytes);
                    //showMessage("Receive: " + strBuf);
                    SystemClock.sleep(1);
                } catch (IOException e) {
                    //showMessage("Socket disconneted");
                    break;
                }
            }
        }

        // 데이터를 소켓으로 전송한다
        public void write(String strBuf) {
            try {
                // 출력 스트림에 데이터를 저장한다
                byte[] buffer = strBuf.getBytes();
                mmOutStream.write(buffer);
                //showMessage("Send: " + strBuf);
            } catch (IOException e) {
                //showMessage("Socket write error");
            }
        }
*/
        public void write(byte[] buf){

            /*
            StringBuilder sb = new StringBuilder();
            for(int i=0; i<buf.length; i++){
                //buf[i] = (byte)(buf[i] ^ (0x80));
                //if(buf[i] == 0) buf[i] = 1;
                sb.append(String.format("%02X-", buf[i]));
            }
            Log.d("write",sb.toString());
            */
            try {
                mmOutStream.write(buf);
            } catch (IOException e){}
        }

    }

}
